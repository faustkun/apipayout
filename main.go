package main

import (
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"fmt"
	"os"
	"github.com/labstack/echo/engine/standard"
	"log"
	"net/http"
	"encoding/json"
	"errors"
	"strconv"
	"io"
	"net/url"
)

type Request struct {
	Key string
	MerchantPsw string
	OrderId string // []rune
	Amount uint
	PAN string
	CustomFields string
}

type Response struct {
	Success bool
	OrderId string
	Amount uint
	ErrCode int
}

func (c Response) MarshalJSON() ([]byte, error) {
	if !c.Success {return json.Marshal(struct{Success bool
						 OrderId string
						 Amount uint
						 ErrCode int}{
							Success: c.Success,
							OrderId: c.OrderId,
							Amount: c.Amount})}
	return json.Marshal(struct{Success bool
				   OrderId string
				   Amount uint}{
		Success: c.Success,
		OrderId: c.OrderId,
		Amount: c.Amount})
}
const ValidKey = "validkey"
const ValidPAN = "4111111111111111"
const ValidMerchantPsw = "ValidMerchantPsw"

func ParseUrlEncoded(s string) {
	var Url *url.URL
	Url, err := url.Parse("http://www.example.com/?" + s)
	if err != nil {
		fmt.Println("Can't decode urlEncoded string ")
	}
	fmt.Println("decoded urlEncoded string", Url.Query())
}

func Payout(body io.Reader) (Response, error) {
	var req Request
	var resp Response
	decoder := json.NewDecoder(body)
	err := decoder.Decode(&req)
	if err != nil{
		fmt.Println(err)
		return Response{}, errors.New("Can't decode parameters")
	}
	ParseUrlEncoded(req.CustomFields)
	// проверяем ограничения на данные
	if len(req.OrderId) > 50 {return Response{}, errors.New("OrderingID is not valid")}
	_, err = strconv.ParseUint(req.PAN, 10, 64)
	if err != nil || len(req.PAN) != 16 {return Response{}, errors.New("PAN is not valid")}
	// проверяем валидность запроса
	if req.Key == ValidKey && req.PAN == ValidPAN && req.MerchantPsw == ValidMerchantPsw {
		resp = Response{
			Success: true,
			OrderId: req.OrderId,
			Amount: req.Amount,
		}
	} else {
		resp = Response{
			Success: false,
			OrderId: req.OrderId,
			ErrCode: 1,
		}
	}

	return resp, nil
}

func main(){
	server := echo.New()
	server.Use(middleware.Recover())
	server.Use(middleware.Gzip())

	server.POST("/api/Payout", func(c echo.Context) error {
		res, err := Payout(c.Request().Body())
		if err != nil {
			log.Println(err)
			return c.JSON(http.StatusBadRequest, Response{ErrCode: 1})
		}
		return c.JSON(http.StatusOK, res)
	})

	fmt.Println("Server started")
	server.Run(standard.New(":3000"))

	log.Printf("Server on %s stopped\n", 3000)
	os.Exit(0)
}
