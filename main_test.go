package main

import (
	"testing"
	"strings"
	"errors"
	"fmt"
)


func TestPayout(t *testing.T) {
	// абсолютно не валидные данные
	r := strings.NewReader("")
	_, err := Payout(r)
	if err == nil {
		t.Error(errors.New("Incorrect decoding input data"))
	}
	// не валидный идентификатор операции
	r = strings.NewReader(`{
	"Key": "validkey",
	"MerchantPsw":
	"ValidMerchantPsw",
	"OrderId": "123456789012345678901234567890123456789012345678901",
	"Amount": 1234,
	"PAN": "4111111111111111",
	"CustomFields": ""}`)
	_, err = Payout(r)
	if err.Error() != "OrderingID is not valid" {
		t.Error(err)
	}
	// не валидная сумма для блокировки
	r = strings.NewReader(`{
	"Key": "validkey",
	"MerchantPsw":
	"ValidMerchantPsw",
	"OrderId": "123",
	"Amount": 1234.1,
	"PAN": "4111111111111111",
	"CustomFields": ""}`)
	_, err = Payout(r)
	if err.Error() != "Can't decode parameters" {
		t.Error(err)
	}
	// не валидный PAN
	r = strings.NewReader(`{
	"Key": "validkey",
	"MerchantPsw":
	"ValidMerchantPsw",
	"OrderId": "123",
	"Amount": 1234.1,
	"PAN": "41111111111111113",
	"CustomFields": ""}`)
	_, err = Payout(r)
	if err.Error() != "Can't decode parameters" {
		t.Error(err)
	}
}

func TestResponse_MarshalJSON(t *testing.T) {
	r := Response{Success: true}
	s, _ := r.MarshalJSON()
	if strings.Contains(string(s), "ErrCode") {
		fmt.Println(string(s))
		t.Error(errors.New("Incorect marshal to jsom Response struct"))
	}

	r = Response{Success: false}
	s, _ = r.MarshalJSON()
	if !strings.Contains(string(s), "ErrCode") {
		fmt.Println(string(s))
		t.Error(errors.New("Incorect marshal to jsom Response struct"))
	}
}

func TestPayout2(t *testing.T) {
	// всё правильно
	r := strings.NewReader(`{
	"Key": "validkey",
	"MerchantPsw":
	"ValidMerchantPsw",
	"OrderId": "123",
	"Amount": 1234,
	"PAN": "4111111111111111",
	"CustomFields": ""}`)
	res, _ := Payout(r)
	if res.Success != true {t.Error(errors.New("Vrong working Payout"))}
	// неправильная карта
	r = strings.NewReader(`{
	"Key": "validkey",
	"MerchantPsw":
	"ValidMerchantPsw",
	"OrderId": "123",
	"Amount": 1234,
	"PAN": "4111111111111112",
	"CustomFields": ""}`)
	res, _ = Payout(r)
	if res.Success != false {t.Error(errors.New("Vrong working Payout"))}

	// неправильный пароль
	r = strings.NewReader(`{
	"Key": "validkey",
	"MerchantPsw": "ValidMerchantPsw2",
	"OrderId": "123",
	"Amount": 1234,
	"PAN": "4111111111111111",
	"CustomFields": ""}`)
	res, _ = Payout(r)
	if res.Success != false {t.Error(errors.New("Vrong working Payout"))}

	// неправильный ключ
	r = strings.NewReader(`{
	"Key": "validkey",
	"MerchantPsw":
	"ValidMerchantPsw",
	"OrderId": "123",
	"Amount": 1234.1,
	"PAN": "41111111111111113",
	"CustomFields": ""}`)
	res, _ = Payout(r)
	if res.Success != false {t.Error(errors.New("Vrong working Payout"))}

}